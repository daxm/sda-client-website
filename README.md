NOTE: THE X86 STEPS HAVE NOT BEEN VALIDATED YET.

To run as a Docker container use:  

On x86 platforms (AKA MAC or PC):
```bash
  sudo docker rm -f sdapov
  sudo docker rmi -f sdawebsite
  sudo docker build -f Dockerfile.x86 -t sdawebsite .
  sudo docker run --restart=unless-stopped -p 80:80 -d --name sdapov sdawebsite
```

On ARM platforms (AKA Raspberry Pi):
```bash
  sudo docker rm -f sdapov
  sudo docker rmi -f sdawebsite
  sudo docker build -f Dockerfile.arm -t sdawebsite .
  sudo docker run --restart=unless-stopped -p 80:80 -d --name sdapov sdawebsite
```


OR use one of the provide shell scripts (that should contain the commands shown above):
On x86:
```bash
 sudo docker-x86.sh
```

On ARM:
```bash
 sudo docker-arm.sh
```
