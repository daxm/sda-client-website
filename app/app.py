from datetime import datetime
import sys
import os
from flask import Flask, request, render_template
app = Flask(__name__)

try:
    with open('/hostname', 'r') as f:
        hostname = f.read()
except OSError:
    hostname = 'UNKNOWN'

@app.route('/')
def index():
    data = {'ip_address': request.environ.get('HTTP_X_REAL_IP', request.remote_addr),
            'time': datetime.now().strftime("%d %B, %Y %X"),
            'hostname': hostname}
    return render_template('index.html', **data)

if __name__ == '__main__':
   app.run(host='0.0.0.0', debug = True, port=80)
